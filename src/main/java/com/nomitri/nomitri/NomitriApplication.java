package com.nomitri.nomitri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class NomitriApplication {

	public static void main(String[] args) {
		SpringApplication.run(NomitriApplication.class, args);
	}


	@GetMapping("/nomitri")
	public String sayHello(){
		return "Hello Nomitri";
	}
}
