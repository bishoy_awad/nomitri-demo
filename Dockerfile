FROM    openjdk:14.0.2
WORKDIR /usr/app/
COPY   ./target/nomitri-*.jar application.jar
CMD     ["java","-jar","application.jar"]
EXPOSE  9080
